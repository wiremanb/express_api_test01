var Customer = require('./Schemas/customer');
var User = require('./Schemas/user');

exports.createCustomer = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {

        // no user found
        if(err)
            return res.status(500).send({success: false, message: "Invalid user"});

        // Check to see if user trying to create new user is admin
        if(user.permissions.admin) {
            if (req.body.customername) {
                var newCustomer = new Customer.model(JSON.parse(req.body.newcustomer));

                // save the user and check for errors
                newCustomer.save(function (err) {
                    if (err)
                        return res.status(500).send({success: false, message: ""});
                    res.json({success: true, message: 'Customer created!'});
                    next();
                });
            }
            else
                res.status(500).json({success: false, message: "Error creating customer"});
        }
        // User is not admin
        else {
            return res.status(403).send({success: false, message: "Wrong permissions"});
        }
    });
};

exports.getCustomerSchema = function(req, res, next) {
    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin
        if(user.permissions.admin) {
            var customer = new Customer.model();
            customer.id = "";
            customer.name = "";
            customer.event_date = new Date();
            customer.last_edited = new Date();
            customer.customer_poc = [];
            customer.assessments = [];
            customer.analyst_name = "";

            res.json({success: true, message: "", customer: customer});

            // next();
            // Customer.findOne({name: req.params.customername}, function (err, customer) {
            //     if (err)
            //         return res.status(500).send({success: false, message: ""});
            //     else {
            //         res.json({success: true, message: "", customer: customer});
            //         next();
            //     }
            // });
        }

        // Not admin and not same user
        else {
            return res.status(500).send({success: false, message: ""});
        }
    });
};

exports.getCustomers = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        if(user.permissions.admin) {
            // console.log(`res: ${res.toString()}`);
            Customer.model.find(function (err, customers) {
                if (err)
                    return res.status(500).send({success: false, message: ""});
                else {
                    res.json({success: true, message: "", customers: customers});
                    next();
                }
            });
        } else {
            return res.status(500).send({success: false, message: ""});
        }
    });
};

exports.getCustomer = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin
        if(user.permissions.admin) {
            Customer.model.findOne({name: req.params.customername}, function (err, customer) {
                if (err)
                    return res.status(500).send({success: false, message: ""});
                else {
                    res.json({success: true, message: "", customer: customer});
                    next();
                }
            });
        }

        // Not admin and not same user
        else {
            return res.status(500).send({success: false, message: ""});
        }
    });
};

exports.updateCustomer = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin or same user
        if(user.permissions.admin) {
            // Find user
            Customer.model.findByIdAndUpdate(req.params.customerId, req.body, {new: true}, function (err, customer) {
                if (err)
                    return res.status(500).send({success: false, message: err.message});
                res.json({success: true, message: 'Customer updated!', customer: customer});
                next();
            });
        }
        // Not admin and not same user
        else {
            return res.status(500).send({success: false, message: ""});
        }
    });
};

exports.removeCustomer = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin
        if(user.permissions.admin) {
            Customer.model.deleteOne({_id: req.params.customerId}, function (err, customer) {
                if (err)
                    return res.status(500).send({success: false, message: ""});

                res.json({success: true, message: 'Successfully deleted customer!'});
                next();
            });
        }
        // User not admin
        else
            return res.status(500).send({success: false, message: "Missing permissions"});
    });
};

exports.removeAllCustomers = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin
        if(user.permissions.admin) {
            Customer.model.deleteMany({}, function (err, customer) {
                if (err)
                    return res.status(500).send({success: false, message: ""});

                res.json({success: true, message: 'Successfully deleted customers!'});
            });
        }
        // User not admin
        else
            return res.status(500).send({success: false, message: "Missing permissions"});
    });
};
