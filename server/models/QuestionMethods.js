var Question = require('./Schemas/question');
var User = require('./Schemas/user');

exports.createQuestion = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {

        // no user found
        if(err)
            return res.status(500).send({success: false, message: "No user found"});

        // Check to see if user trying to create new user is admin
        if(user.permissions.admin) {
            if (req.body.question) {
                var newQuestion = new Question.model(req.body.question);

                // save the user and check for errors
                newQuestion.save(function (err, question) {
                    if (err)
                        return res.status(500).send({success: false, message: "Failed to save question"});
                    res.json({success: true, message: 'Question created!', question: question});
                    // next();
                });
            }
            else
                res.status(500).json({success: false, message: "Error creating question"});
        }
        // User is not admin
        else {
            return res.status(403).send({success: false, message: "Error creating question"});
        }
    });
};

exports.createQuestionSet = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {

        // no user found
        if(err)
            return res.status(500).send({success: false, message: ""});

        // Check to see if user trying to create new user is admin
        if(user.permissions.admin) {
            if (req.body.question_set) {
                // console.log("question_set found!");
                // console.log("question_set received: " + JSON.stringify(req.body.question_set));
                var newQuestionSet = new Question.questionSetModel(req.body.question_set);

                // save the user and check for errors
                newQuestionSet.save(function (err, questionSet) {
                    if (err)
                        return res.status(500).send({success: false, message: ""});
                    res.json({success: true, message: 'Question Set created!', questionSet: questionSet});
                });
            }
            else
                res.status(500).json({success: false, message: "Error creating question set"});
        }
        // User is not admin
        else {
            return res.status(403).send({success: false, message: ""});
        }
    });
};

exports.getQuestionSchema = function(req, res, next) {
    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin
        if(user.permissions.admin) {
            var question = new Question.model();
            var childQuestion = new Question.childModel();

            question.id = "";
            question.name = "";
            question.event_date = new Date();
            question.last_edited = new Date();
            question.customer_name = "";
            question.analyst_name = "";
            question.question = "";
            question.information = "";
            question.type = "";
            question.skip = false;
            question.answer.correct.text = "";
            question.answer.correct.value = false;
            question.answer.incorrect.text = "";
            question.answer.incorrect.value = true;
            question.notes = "";
            question.weight = 0.00;

            childQuestion.id = "";
            childQuestion.name = "";
            childQuestion.event_date = new Date();
            childQuestion.last_edited = new Date();
            childQuestion.customer_name = "";
            childQuestion.analyst_name = "";
            childQuestion.question = "";
            childQuestion.information = "";
            childQuestion.type = "";
            childQuestion.skip = false;
            childQuestion.answer = "";
            childQuestion.notes = "";
            childQuestion.weight = 1.25;

            question.children.push(childQuestion);

            res.json({success: true, message: "", question: question});
        }

        // Not admin and not same user
        else {
            return res.status(500).send({success: false, message: ""});
        }
    });
};

exports.getQuestionSetSchema = function(req, res, next) {
    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: "No user found"});

        // If user is admin
        if(user.permissions.admin) {
            var questionSet = new Question.questionSetModel();
            var question = new Question.model();
            var childQuestion = new Question.childModel();

            question.id = "";
            question.name = "";
            question.event_date = new Date();
            question.last_edited = new Date();
            question.customer_name = "";
            question.analyst_name = "";
            question.question = "";
            question.information = "";
            question.type = "";
            question.skip = false;
            question.answer.correct.text = "";
            question.answer.correct.value = false;
            question.answer.incorrect.text = "";
            question.answer.incorrect.value = true;
            question.notes = "";
            question.weight = 0.00;

            childQuestion.id = "";
            childQuestion.name = "";
            childQuestion.event_date = new Date();
            childQuestion.last_edited = new Date();
            childQuestion.customer_name = "";
            childQuestion.analyst_name = "";
            childQuestion.question = "";
            childQuestion.information = "";
            childQuestion.type = "";
            childQuestion.skip = false;
            childQuestion.answer = "";
            childQuestion.notes = "";
            childQuestion.weight = 1.25;

            question.children.push(childQuestion);

            questionSet.name = "";
            questionSet.id = "";
            questionSet.creation_date = new Date();
            questionSet.last_edited = new Date();
            questionSet.created_by = "";
            questionSet.edited_by = "";
            questionSet.questions.push(question);


            res.json({success: true, message: "", questionSet: questionSet});
        }

        // Not admin and not same user
        else {
            return res.status(500).send({success: false, message: "Incorrect permissions"});
        }
    });
};

exports.getQuestions = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        if(user.permissions.admin) {
            // console.log(`res: ${res.toString()}`);
            Question.model.find(function (err, questions) {
                if (err)
                    return res.status(500).send({success: false, message: ""});
                else {
                    res.json({success: true, message: "", questions: questions});
                }
            });
        } else {
            return res.status(500).send({success: false, message: ""});
        }
    });
};

exports.getQuestionSets = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        if(user.permissions.admin) {
            // console.log(`res: ${res.toString()}`);
            Question.questionSetModel.find(function (err, questionSets) {
                if (err)
                    return res.status(500).send({success: false, message: ""});
                else {
                    res.json({success: true, message: "", questionSets: questionSets});
                }
            });
        } else {
            return res.status(500).send({success: false, message: ""});
        }
    });
};

exports.getQuestion = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin
        if(user.permissions.admin) {
            Question.model.findOne({name: req.params.questionId}, function (err, question) {
                if (err)
                    return res.status(500).send({success: false, message: ""});
                else {
                    res.json({success: true, message: "", question: question});
                }
            });
        }

        // Not admin and not same user
        else {
            return res.status(500).send({success: false, message: ""});
        }
    });
};

exports.getQuestionSet = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin
        if(user.permissions.admin) {
            Question.questionSetModel.findOne({name: req.params.questionSetId}, function (err, questionSet) {
                if (err)
                    return res.status(500).send({success: false, message: "Couldn't find question set"});
                else {
                    res.json({success: true, message: "", questionSet: questionSet});
                }
            });
        }

        // Not admin and not same user
        else {
            return res.status(500).send({success: false, message: ""});
        }
    });
};

exports.updateQuestion = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin or same user
        if(user.permissions.admin) {
            // Find user
            Question.model.findByIdAndUpdate(req.params.questionId, req.body, {new: true}, function (err, question) {
                if (err)
                    return res.status(500).send({success: false, message: err.message});
                res.json({success: true, message: 'Question updated!', question: question});
            });
        }
        // Not admin and not same user
        else {
            return res.status(500).send({success: false, message: ""});
        }
    });
};

exports.updateQuestionSet = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin or same user
        if(user.permissions.admin) {
            // Find user
            Question.questionSetModel.findByIdAndUpdate(req.params.questionSetId, req.body, {new: true}, function (err, questionSet) {
                if (err)
                    return res.status(500).send({success: false, message: err.message});
                res.json({success: true, message: 'Question set updated!', questionSet: questionSet});
            });
        }
        // Not admin and not same user
        else {
            return res.status(500).send({success: false, message: "Incorrect permissions"});
        }
    });
};

exports.removeQuestion = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin
        if(user.permissions.admin) {
            Question.model.deleteOne({_id: req.params.questionId}, function (err, question) {
                if (err)
                    return res.status(500).send({success: false, message: ""});

                res.json({success: true, message: 'Successfully deleted question!'});
            });
        }
        // User not admin
        else
            return res.status(500).send({success: false, message: "Missing permissions"});
    });
};

exports.removeQuestionSet = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin
        if(user.permissions.admin) {
            Question.questionSetModel.deleteOne({_id: req.params.questionSetId}, function (err, questionSet) {
                if (err)
                    return res.status(500).send({success: false, message: ""});

                res.json({success: true, message: 'Successfully deleted question set!'});
            });
        }
        // User not admin
        else
            return res.status(500).send({success: false, message: "Missing permissions"});
    });
};

exports.removeAllQuestions = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin
        if(user.permissions.admin) {
            Question.model.deleteMany({}, function (err, question) {
                if (err)
                    return res.status(500).send({success: false, message: ""});

                res.json({success: true, message: 'Successfully deleted questions!'});
            });
        }
        // User not admin
        else
            return res.status(500).send({success: false, message: "Missing permissions"});
    });
};

exports.removeAllQuestionSets = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: "Can't find user"});

        // If user is admin
        if(user.permissions.admin) {
            Question.questionSetModel.deleteMany({}, function (err, questionSet) {
                if (err)
                    return res.status(500).send({success: false, message: "Can't find any question sets"});

                res.json({success: true, message: 'Successfully deleted question sets!'});
            });
        }
        // User not admin
        else
            return res.status(500).send({success: false, message: "Missing permissions"});
    });
};
