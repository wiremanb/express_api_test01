var User = require('./Schemas/user');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
const config = require('../config');
require('datejs');

exports.login = function(req, res, next) {

    User.findOne({username: req.body.username}, function(err, user) {

        if(err)
            res.status(403).json({success: false, message: "Invalid login"})

        if(user) {
            bcrypt.compare(req.body.password, user.password, function (err, comp) {
                if (comp) {
                    const payload = {team: user.team};
                    // Creates token valid for 5hrs
                    var token = jwt.sign(payload, config.secret, {expiresIn: 60*60*5});

                    user.tokenValue = token;
                    user.tokenIssuedTime = new Date().getTime();
                    user.tokenEndTime = new Date(user.tokenIssuedTime).addHours(5);
                    user.lastLogIn = new Date().getTime();

                    // save the user and check for errors
                    user.save(function (err) {
                        if (err)
                            res.status(403).json({success: false, message: "Invalid login"})

                        res.json({success: true, message: "Successful login", token: token, id: user._id});
                        next();
                    });
                } else {
                    res.status(403).json({success: false, message: "Invalid login"})
                }
            });
        }
        else
            res.status(403).json({success: false, message: "Invalid login"})
    })
};

exports.logout = function(req, res, next) {

    var token = req.headers['x-access-token'];
    var userId = req.headers['x-access-id'];

    // decode token
    if (token && userId) {
        // verifies secret and checks exp
        jwt.verify(token, config.secret, function (err, decoded) {
            if (err) {
                return res.status(500).send({
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            }

            // Grab user context
            User.findById({_id: userId}, function (err, user) {
                if (err)
                    return res.status(500).json({success: false, message: "No valid session"});
                try {
                    if (user.tokenValue != null && token === user.tokenValue) {
                        // Set token values to nothing. This will signal a logout to the checkToken function.
                        user.tokenValue = '';
                        user.tokenIssuedTime = 0;
                        user.tokenEndTime = 0;
                        user.lastLogOut = new Date().getTime();

                        user.save(function (err) {
                            if (err)
                                res.send(err);
                            res.json({success: true, message: 'Logged out!'});
                            next();
                        });
                    }
                    else {
                        return res.status(500).send({success: false, message: 'No valid session'});
                    }
                } catch (err) {
                    console.log(`error: ${err.message}`);
                    return res.status(500).json({success: false, message: "No valid session"});
                }
            });
        });
    } else {
        return res.status(403).send({success: false, message: 'No valid session'});
    }
};

exports.checkToken = function(req, res, next) {

    // check header or url parameters or post parameters for token
    // var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var token = req.headers['x-access-token'];
    var userId = req.headers['x-access-id'];

    // decode token
    if (token && userId) {
        // verifies secret and checks exp
        jwt.verify(token, config.secret, function (err, decoded) {
            if (err) {
                return res.status(500).send({success: false, message: 'Failed to authenticate token.'});
            }

            User.findById({_id: userId}, function (err, user) {
                if (err) {
                    return res.status(500).send({success: false, message: 'Failed to authenticate token. -> couldn\'t find user'});
                }

                try {
                    if (user.tokenValue != null && token === user.tokenValue) {
                        // console.log(`decoded: ${decoded.team}`);
                        next();
                        // return res.status(302).send({success: true, message: "Valid", userPermissions: decoded});
                    }
                    else {
                        return res.status(500).send({success: false, message: 'Failed to authenticate token. -> using wrong token'});
                    }
                } catch(err) {
                    // console.log(`error: ${err.message}`);
                    return res.status(500).send({success: false, message: 'Failed to authenticate token. -> couldn\'t find user'});
                }
            });
        });
    } else {
        return res.status(403).send({success: false, message: 'Missing id or token'});
    }
};