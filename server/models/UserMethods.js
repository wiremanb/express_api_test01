var User = require('./Schemas/user');
var Auth = require('../models/AuthMethods');
var bcrypt = require('bcrypt');

exports.createUser = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {

        // no user found
        if(err)
            return res.status(500).send({success: false, message: ""});

        // Check to see if user trying to create new user is admin
        if(user.permissions.admin) {
            if (req.body.username && req.body.password && req.body.team) {
                var newUser = new User();
                newUser.username = req.body.username;
                bcrypt.hash(req.body.password, 10, function (err, hash) {
                    if (err)
                        return res.status(500).send({success: false, message: ""});
                    else {
                        newUser.password = hash;
                        newUser.team = req.body.team;
                        try {
                            newUser.permissions.admin = req.body.admin;
                            newUser.permissions.canView = req.body.canView;
                            newUser.permissions.canEdit = req.body.canEdit;
                        } catch(err) {
                            console.log("no permissions added...");
                        }
                        // save the user and check for errors
                        newUser.save(function (err) {
                            if (err)
                                return res.status(500).send({success: false, message: ""});
                            res.json({success: true, message: 'User created!'});
                            next();
                        });
                    }
                });
            }
            else
                res.json({success: false, message: "Error creating user"});
        }
        // User is not admin
        else {
            return res.status(500).send({success: false, message: ""});
        }
    });
};

exports.getUsers = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // Not admin... so return only users on team
        if(!user.permissions.admin && user.permissions.canView) {
            // console.log(`res: ${res.toString()}`);
            User.find({team: user.team}, function (err, users) {
                if (err)
                    return res.status(500).send({success: false, message: ""});
                else {
                    res.json({success: true, message: "", users: users});
                    next();
                }
            });
        } else if(user.permissions.admin) {
            // console.log(`res: ${res.toString()}`);
            User.find(function (err, users) {
                if (err)
                    return res.status(500).send({success: false, message: ""});
                else {
                    res.json({success: true, message: "", users: users});
                    next();
                }
            });
        } else {
            return res.status(500).send({success: false, message: ""});
        }
    });
};

exports.getUser = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin or same user
        if(user.permissions.admin || user._id === req.params.userId) {
            User.findOne({_id: req.params.userId}, function (err, users) {
                if (err)
                    return res.status(500).send({success: false, message: ""});
                else {
                    res.json({success: true, message: "", users: users});
                    next();
                }
            });
        }
        // Not admin and not same user
        else {
            return res.status(500).send({success: false, message: ""});
        }
    });
};

exports.updateUser = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin or same user
        if(user.permissions.admin || user._id === req.params.userId) {
            // Find user
            User.findById(req.params.userId, function (err, updateUser) {
                if (err)
                    res.send(err);

                if (req.body.name)
                    updateUser.name = req.body.name;

                // -----------PERMISSIONS-----------
                if (req.body.admin)
                    updateUser.permissions.admin = req.body.admin;

                if (req.body.canView)
                    updateUser.permissions.canView = req.body.canView;

                if (req.body.canEdit)
                    updateUser.permissions.canEdit = req.body.canEdit;
                // -----------END PERMISSIONS-----------

                // TODO: Implement two-factor
                if (req.body.password)
                    updateUser.password = req.body.password;

                if (req.body.team)
                    updateUser.team = req.body.team;

                // save the user and check for errors
                user.save(function (err) {
                    if (err)
                        return res.status(500).send({success: false, message: ""});
                    res.json({success: true, message: 'User updated!'});
                    next();
                });

            });
        }
        // Not admin and not same user
        else {
            return res.status(500).send({success: false, message: ""});
        }
    });
};

exports.removeUser = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin
        if(user.permissions.admin) {
            User.deleteOne({_id: req.params.userId}, function (err, user) {
                if (err)
                    return res.status(500).send({success: false, message: ""});

                res.json({success: true, message: 'Successfully deleted user!'});
                next();
            });
        }
        // User not admin
        else
            return res.status(500).send({success: false, message: "Missing permissions"});
    });
};

exports.removeAllUsers = function(req, res, next) {

    var userId = req.headers['x-access-id'];

    User.findById({_id: userId}, function(err, user) {
        if(err)
            return res.status(500).send({success: false, message: ""});

        // If user is admin
        if(user.permissions.admin) {
            User.deleteMany({username: {$ne: 'admin'}}, function (err, user) {
                if (err)
                    return res.status(500).send({success: false, message: ""});

                res.json({success: true, message: 'Successfully deleted users!'});
                next();
            });
        }
        // User not admin
        else
            return res.status(500).send({success: false, message: "Missing permissions"});
    });
};
