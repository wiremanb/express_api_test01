var mongoose = require('mongoose');
var question = require('./question');
var customer = require('./customer');

var AssessmentSchema = new mongoose.Schema({
    name: String,
    id: String,
    event_date: Date,
    created_date: Date,
    last_edited: Date,
    created_by: String,
    analyst_name: String,
    notes: String,
    customer: String,
    questionSet: question.questionSetSchema
});

module.exports = {model: mongoose.model('assessment', AssessmentSchema), schema: AssessmentSchema};