var mongoose = require('mongoose');

var CustomerPOCSchema   = new mongoose.Schema({
    name: String,
    type: String,
    id: String,
    last_edited: Date,
    address: String,
    email: String,
    primary: Boolean,
    phone: {office: String, cell: String}
});

module.exports = {model: mongoose.model('customer_poc', CustomerPOCSchema), schema: CustomerPOCSchema};