var mongoose = require('mongoose');

var TicketSchema   = new mongoose.Schema({
    ticketName: String,
    ticketAssignedTo: String,
    ticketCreated: {type: Date, default: new Date().getTime()},
    ticketStatus: {type: String, default: 'Open'},
    ticketClosed: Date,
    ticketMessage: {type: String, default: ""},
    ticketType: String,
    ticketCorrespondence: {
        customer: {type: [String], default: [""]},
        provider: {type: [String], default: [""]}
    }
});

module.exports = mongoose.model('ticket', TicketSchema);