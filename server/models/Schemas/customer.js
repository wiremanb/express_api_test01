var mongoose = require('mongoose');
var assessment = require('./assessment');
var customer_poc = require('./customer_poc');

var CustomerSchema   = new mongoose.Schema({
    name: String,
    id: String,
    event_date: Date,
    last_edited: Date,
    analyst_name: String,
    assessments: [assessment.schema],
    customer_poc: [customer_poc.schema]
});

module.exports = {model: mongoose.model('customer', CustomerSchema), schema: CustomerSchema};