var mongoose = require('mongoose');

// Some questions can have a correct value of no
// Questions that have child questions can have yes or no answers and depending on the answer
// a different set of questions can be asked... next question that we need to ask.

var ChildSchema = new mongoose.Schema({
    name: String,
    id: String,
    event_date: Date,
    last_edited: Date,
    analyst_name: String,
    customer_name: String,
    question: String,
    information: String,
    type: String,
    skip: Boolean,
    answer: Boolean,
    notes: String,
    weight: Number
});

var QuestionSchema = new mongoose.Schema({
    name: String,
    id: String,
    event_date: Date,
    last_edited: Date,
    creation_date: Date,
    analyst_name: String,
    customer_name: String,
    question: String,
    information: String,
    type: String,
    skip: Boolean,
    answer: {correct: {text: String, value: Boolean}, incorrect: {text: String, value: Boolean}},
    notes: String,
    weight: Number,
    children: [ChildSchema]
});

var QuestionSetSchema = new mongoose.Schema({
    name: String,
    id: String,
    creation_date: Date,
    last_edited: Date,
    created_by: String,
    edited_by: String,
    questions: [QuestionSchema]
});

module.exports = {
    questionSetModel: mongoose.model('questionSet', QuestionSetSchema), questionSetSchema: QuestionSetSchema,
    model: mongoose.model('question', QuestionSchema), schema: QuestionSchema,
    childModel:mongoose.model('childQuestion', ChildSchema), childSchema: ChildSchema
};