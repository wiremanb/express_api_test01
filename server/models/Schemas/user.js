var mongoose = require('mongoose');

var UserSchema   = new mongoose.Schema({
    name: String,
    username: String,
    password: String,
    lastLogIn: Date,
    lastLogOut: Date,
    tokenValue: {type: String, default: ''},
    tokenIssuedTime: Date,
    tokenEndTime: Date,
    team: {type: String, default: ''},
    permissions: {
        admin: {type: Boolean, default: false},
        canEdit: {type: Boolean, default: false},
        canView: {type: Boolean, default: false}
    }
});

module.exports = mongoose.model('user', UserSchema);