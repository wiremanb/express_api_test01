var Auth = require('../models/AuthMethods');

module.exports = (router, app, UserMethods) => {

    router.post('/create', Auth.checkToken, UserMethods.createUser);

    router.get('/', Auth.checkToken, UserMethods.getUsers);

    router.put('/:userId', Auth.checkToken, UserMethods.updateUser);

    router.get('/:userId', Auth.checkToken, UserMethods.getUser);

    router.delete('/all', Auth.checkToken, UserMethods.removeAllUsers);

    router.delete('/:userId', Auth.checkToken, UserMethods.removeUser);

    return router;
}