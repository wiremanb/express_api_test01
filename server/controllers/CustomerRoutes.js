var Auth = require('../models/AuthMethods');

module.exports = (router, app, CustomerMethods) => {

    router.post('/create', Auth.checkToken, CustomerMethods.createCustomer);

    router.get('/', Auth.checkToken, CustomerMethods.getCustomers);

    router.get('/schema', Auth.checkToken, CustomerMethods.getCustomerSchema);

    router.put('/:customerId', Auth.checkToken, CustomerMethods.updateCustomer);

    router.get('/:customerId', Auth.checkToken, CustomerMethods.getCustomer);

    router.delete('/all', Auth.checkToken, CustomerMethods.removeAllCustomers);

    router.delete('/:customerId', Auth.checkToken, CustomerMethods.removeCustomer);

    return router;
}