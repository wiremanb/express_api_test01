var Auth = require('../models/AuthMethods');

module.exports = (router, app, QuestionMethods) => {

    router.post('/create/set', Auth.checkToken, QuestionMethods.createQuestionSet);

    router.post('/create', Auth.checkToken, QuestionMethods.createQuestion);

    // router.post('/:questionId/child/create', Auth.checkToken, QuestionMethods.createChildQuestion);

    router.get('/', Auth.checkToken, QuestionMethods.getQuestions);

    router.get('/sets', Auth.checkToken, QuestionMethods.getQuestionSets);

    router.get('/schema', Auth.checkToken, QuestionMethods.getQuestionSchema);

    router.get('/set/schema', Auth.checkToken, QuestionMethods.getQuestionSetSchema);

    router.put('/:questionId', Auth.checkToken, QuestionMethods.updateQuestion);

    router.put('/set/:questionSetId', Auth.checkToken, QuestionMethods.updateQuestionSet);

    router.get('/:questionId', Auth.checkToken, QuestionMethods.getQuestion);

    router.delete('/all', Auth.checkToken, QuestionMethods.removeAllQuestions);

    router.delete('/set/all', Auth.checkToken, QuestionMethods.removeAllQuestionSets);

    router.delete('/:questionId', Auth.checkToken, QuestionMethods.removeQuestion);

    router.delete('/set/:questionSetId', Auth.checkToken, QuestionMethods.removeQuestionSet);

    return router;
}