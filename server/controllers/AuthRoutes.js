module.exports = (router, app, AuthMethods) => {

    /**
     * @swagger
     * /api/auth/login:
     *   post:
     *     tags:
     *       - auth
     *     description: Performs login
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: username
     *         description: username
     *         in: request
     *         required: true
     *       - name: password
     *         description: password
     *         in: request
     *         required: true
     *     responses:
     *       200:
     *         description: Just to be thorough
     */
    router.post('/login', AuthMethods.login);
    router.post('/logout', AuthMethods.logout);

    router.get('/');
    // router.put('/:userId', UserMethods.updateUser);
    // router.get('/:userId', UserMethods.getUser);
    // router.delete('/:userId', UserMethods.removeUser);
    return router;
}