const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const config = require('./config');
const cors = require('cors');
const swaggerJSDoc = require('swagger-jsdoc');

// SETUP ------------------------------------------
const port = 3000
const app = express();
app.set('superSecret', config.secret);
app.use(cors());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

// swagger definition
// var swaggerDefinition = {
//     info: {
//         title: 'Node Swagger API',
//         version: '1.0.0',
//         description: 'Demonstrating how to describe a RESTful API with Swagger',
//     },
//     host: 'localhost:3000',
//     basePath: '/'
// };
// options for the swagger docs
// var options = {
//     // import swaggerDefinitions
//     swaggerDefinition: swaggerDefinition,
//     // path to the API docs
//     apis: ['./controllers/AuthRoutes.js'],// pass all in array
// };
// initialize swagger-jsdoc
// var swaggerSpec = swaggerJSDoc(options);

// Setup mongoose
var mongoDB = 'mongodb://127.0.0.1/test01';
mongoose.connect(mongoDB, { useNewUrlParser: true });
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;
//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// const defaultMethods = require('./models/defaultmethods');
// const defaultRoutes = require('./controllers/defaultrouter')(express.Router(), app, defaultMethods);
const UserMethods = require('./models/UserMethods');
const UserRoutes = require('./controllers/UserRoutes')(express.Router(), app, UserMethods);
const CustomerMethods = require('./models/CustomerMethods');
const CustomerRoutes = require('./controllers/CustomerRoutes')(express.Router(), app, CustomerMethods);
const QuestionMethods = require('./models/QuestionMethods');
const QuestionRoutes = require('./controllers/QuestionRoutes')(express.Router(), app, QuestionMethods);
const AuthMethods = require('./models/AuthMethods');
const AuthRoutes = require('./controllers/AuthRoutes')(express.Router(), app, AuthMethods);

// ------------------------------------------------

// REGISTER ROUTES ------------------------------------------
// app.use(defaultRoutes);
app.use('/api/auth', AuthRoutes);
app.use('/api/users', UserRoutes);
app.use('/api/customers', CustomerRoutes);
app.use('/api/questions', QuestionRoutes);

// serve swagger
// app.get('/swagger.json', function(req, res) {
//     res.setHeader('Content-Type', 'application/json');
//     res.send(swaggerSpec);
// });


// START SERVER ------------------------------------------
app.listen(port,() => console.log(`Example app listening on port ${port}!`));
